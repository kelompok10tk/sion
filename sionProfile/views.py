from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from sionLogin.db_helper import DatabaseConnection

def index(request):
	if 'email_login' in request.session:
		return HttpResponseRedirect(reverse('sionProfile:profile'))

	else:
		return HttpResponseRedirect(reverse('sionLogin:index'))

def profile(request):
	if 'email_login' not in request.session.keys():
		return HttpResponseRedirect(reverse('sionLogin:index'))

	conn = DatabaseConnection()
    conn.cursor.execute("select email, nama, alamat from sion.user where email='"+request.session['email_login']+"';")
    row = conn.cursor.fetchone()
    response['email'] = row[0]
    response['nama'] = row[1]
    response['alamat'] = row[2]

    if request.session['role'] == 'donatur':
    	conn.cursor.execute("select saldo from sion.donatur where email='"+request.session['email_login']+"';")
    	row = conn.cursor.fetchone()
    	response['saldo'] = row[0]
        html = 'profilDonatur.html'

    elif request.session['role'] == 'relawan' :
    	conn.cursor.execute("select no_hp, tanggal_lahir from sion.relawan where email='"+request.session['email_login']+"';")
    	row = conn.cursor.fetchone()
    	response['no_hp'] = row[0]
    	response['tanggal_lahir'] = row[1]
        html = 'profilRelawan.html'

    elif request.session['role'] == 'sponsor' :
    	conn.cursor.execute("select logo_sponsor from sion.sponsor where email='"+request.session['email_login']+"';")
    	row = conn.cursor.fetchone()
    	response['logo_sponsor'] = row[0]
        html = 'profilSponsor.html'

    elif request.session['role'] == 'pengurus_organisasi' :
    	conn.cursor.execute("select organisasi from sion.pengurus_organisasi where email='"+request.session['email_login']+"';")
    	row = conn.cursor.fetchone()
    	response['organisasi'] = row[0]
        html = 'profilPengurus.html'

    return render(request, html, response)
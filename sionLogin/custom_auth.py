from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse

from .db_helper import DatabaseConnection
#authentication

def auth_login(request):
        if request.method == "POST":
                email = request.POST['username']
                password = request.POST['password']
                #email = 'a1@gmail.com'
                #password = 'pass1'

                #Create db connection
                conn = DatabaseConnection()
                check = conn.check_account(email,password)
                role = conn.get_role(email)

                if check is True:
                        messages.success(request, 'Login success')
                        request.session['email_login'] = email
                        request.session['password'] = password
                        request.session['role'] = role

                else :
                        messages.error(request, 'Login Failed')

        return HttpResponseRedirect(reverse('sionLogin:index'))


def auth_logout(request):
        request.session.flush()
        messages.info(request, "Anda berhasil logout")
        return HttpResponseRedirect(reverse('sionLogin:index'))
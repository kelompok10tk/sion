from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from .db_helper import DatabaseConnection

# Create your views here.
response = {}

def index(request):
    if 'email_login' in request.session :
        print('logged in')
        response['login'] = True
        return HttpResponseRedirect(reverse('sionProfile:index'))

    else :
        print('not logged in')
        response['login'] = False
        html = 'home.html'
        return render(request, html, response)


def profile(request):
    if 'email_login' not in request.session.keys():
        return HttpResponseRedirect(reverse('fitur1:index'))


    conn = DatabaseConnection()
    conn.cursor.execute("select * from sion.user where email='"+request.session['email_login']+"';")
    row = conn.cursor.fetchone()
    response['email'] = row[0]
    response['nama'] = row[2]
    list_alamat = [x.strip() for x in row[3].split(',')]
    response['alamat'] = list_alamat[0]
    try:
        response['kecamatan'] = list_alamat[1]
        response['kabupaten'] = list_alamat[2]
        response['kode_pos'] = list_alamat[3]
    except:
        #do nothing
        response['kecamatan'] = "-"
        response['kabupaten'] = "-"
        response['kode_pos'] = "-"
        
    response['role'] = request.session['role'].capitalize()
    
    if request.session['role'] == 'donatur' or request.session['role'] == 'relawan' :
        html = 'login_sion/session/profil_donatur_relawan.html'

    elif request.session['role'] == 'sponsor' :
        html = 'login_sion/session/profil_sponsor.html'

    elif request.session['role'] == 'pengurus_organisasi' :
        html = 'login_sion/session/profil_pengurus_organisasi.html'

    return render(request, html, response)
